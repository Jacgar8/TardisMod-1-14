package net.tardis.mod.experimental.advancement;

import net.minecraft.advancements.CriteriaTriggers;
import net.tardis.mod.helper.Helper;

/**
 * Created by Swirtzly
 * on 19/04/2020 @ 12:33
 */
//Now now, let's not get triggered
public class TTriggers {

    public static final GenericTardisTrigger OBTAINED = new GenericTardisTrigger(Helper.createRLString("obtain_tardis"));
    public static final GenericTardisTrigger EXTERIOR_UNLOCK = new GenericTardisTrigger(Helper.createRLString("exterior_unlock"));
    public static final GenericTardisTrigger INTERIOR_UNLOCK = new GenericTardisTrigger(Helper.createRLString("interior_unlock"));
    public static final GenericTardisTrigger TARDIS_MOD = new GenericTardisTrigger(Helper.createRLString("tardis_mod"));
    public static final GenericTardisTrigger DRONE_MISSION_COMPLETE = new GenericTardisTrigger(Helper.createRLString("drone_mission_complete"));
    public static final GenericTardisTrigger DUMMY = new GenericTardisTrigger(Helper.createRLString("dummy"));

    public static void init() {
        CriteriaTriggers.register(OBTAINED);
        CriteriaTriggers.register(TARDIS_MOD);
        CriteriaTriggers.register(DRONE_MISSION_COMPLETE);
        CriteriaTriggers.register(EXTERIOR_UNLOCK);
        CriteriaTriggers.register(INTERIOR_UNLOCK);
        CriteriaTriggers.register(DUMMY);
    }

}
