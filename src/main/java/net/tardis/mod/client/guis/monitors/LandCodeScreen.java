package net.tardis.mod.client.guis.monitors;


import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.network.packets.console.DataTypes;
import net.tardis.mod.network.packets.console.LandCode;
import net.tardis.mod.tileentities.ConsoleTile;

public class LandCodeScreen extends MonitorScreen {

    private final TranslationTextComponent title = new TranslationTextComponent(TardisConstants.Strings.GUI + "land_code.title");
    private final TranslationTextComponent desc = new TranslationTextComponent(TardisConstants.Strings.GUI + "land_code.desc");
    private final TranslationTextComponent land_code_set = new TranslationTextComponent(TardisConstants.Strings.GUI + "land_code.set");
    private final TranslationTextComponent land_code_suggestion_default = new TranslationTextComponent(TardisConstants.Strings.GUI + "land_code.suggestion.default");
    private final TranslationTextComponent land_code_suggestion_current_value = new TranslationTextComponent(TardisConstants.Strings.GUI + "land_code.suggestion.current_value");
    private TextFieldWidget code;
    private Button setCode;
    private Button cancel;

    public LandCodeScreen(IMonitorGui monitor) {
        super(monitor, "land_code");
    }

    @Override
    protected void init() {
        super.init();
        this.buttons.clear();
        int centerX = this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2);
        this.setCode = this.createButton(this.parent.getMinX(), this.parent.getMinY(), land_code_set, but -> {
            Network.sendToServer(new ConsoleUpdateMessage(DataTypes.LAND_CODE, new LandCode(code.getText())));
            onClose();
        });
        this.cancel = this.createButton(this.parent.getMinX(), this.parent.getMinY(), TardisConstants.Translations.GUI_CANCEL, but -> {
            onClose();
        });
        int height = this.font.FONT_HEIGHT + 10;
        this.addButton(code = new TextFieldWidget(font,
                centerX - 75,
                this.parent.getMinY() - parent.getHeight() / 2,
                parent.getWidth() - 50,
                height, new StringTextComponent("")));
        this.addButton(this.setCode);
        this.addButton(this.cancel);
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        int centerX = this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2);
        drawCenteredString(matrixStack, this.font, title.getString(), centerX, this.parent.getMaxY() + 10, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, desc.getString(), centerX, this.parent.getMinY() - (parent.getHeight() / 2) - 15, 0xFFFFFF);
        if (this.code.isFocused()) {
            this.code.setSuggestion("");
        }
        if (this.code.isMouseOver(mouseX, mouseY) && !this.code.isFocused()) {

        } else if (this.code.getText().isEmpty() && !this.code.isFocused()) {
            TileEntity te = this.minecraft.world.getTileEntity(TardisHelper.TARDIS_POS);
            if (te instanceof ConsoleTile && te != null) {
                ConsoleTile console = (ConsoleTile) te;
                this.code.setSuggestion(land_code_suggestion_current_value.getString() + console.getLandingCode());
            } else {
                this.code.setSuggestion(land_code_suggestion_default.getString());
            }
        }
    }

    @Override
    public void onClose() {
        super.onClose();
    }

}
