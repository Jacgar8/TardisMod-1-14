package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.TRenderTypes;
import net.tardis.mod.client.models.exteriors.SteamExteriorModel;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.exteriors.SteampunkExteriorTile;

public class SteamExteriorRenderer extends ExteriorRenderer<SteampunkExteriorTile>{
    
    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/steampunk.png");
    public static final SteamExteriorModel MODEL = new SteamExteriorModel();
    
    
    public SteamExteriorRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
        super(rendererDispatcherIn);
    }

    @Override
    public void renderExterior(SteampunkExteriorTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn, float alpha) {
        matrixStackIn.push();
        matrixStackIn.scale(0.25F, 0.25F, 0.25F);
        matrixStackIn.translate(0, 0.5, 0);
        
        ResourceLocation texture = TEXTURE;
        if(tile.getVariant() != null)
            texture = tile.getVariant().getTexture();
        IVertexBuilder ivertexbuilder = bufferIn.getBuffer(TRenderTypes.getTardis(texture));
        MODEL.render(tile, 0.25F, matrixStackIn, ivertexbuilder, combinedLightIn, OverlayTexture.NO_OVERLAY, alpha);
        
        matrixStackIn.rotate(Vector3f.YN.rotationDegrees(60));
        matrixStackIn.translate(-0.2, -1.23, -12.75 / 16.0);
        WorldText TEXT = new WorldText(0.45F, 0.35F, 1, 0x000000);
        TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, tile.getCustomName());
        
        matrixStackIn.pop();
    }

    

}
