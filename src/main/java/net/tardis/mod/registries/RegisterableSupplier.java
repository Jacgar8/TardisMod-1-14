package net.tardis.mod.registries;

import java.util.function.Supplier;

import net.minecraft.util.ResourceLocation;
@Deprecated
public class RegisterableSupplier<T> implements IRegisterable<T>{

	private ResourceLocation regName;
	private Supplier<T> supp;
	
	public RegisterableSupplier(Supplier<T> supp) {
		this.supp = supp;
	}
	
	@Override
	public T setRegistryName(ResourceLocation regName) {
		this.regName = regName;
		return null;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return regName;
	}
	
	public T get() {
		return supp.get();
	}
	
}
