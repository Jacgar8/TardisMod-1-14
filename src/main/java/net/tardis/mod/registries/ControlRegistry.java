package net.tardis.mod.registries;

import java.util.function.Supplier;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.controls.AbstractControl;
import net.tardis.mod.controls.BaseControl;
import net.tardis.mod.controls.CommunicatorControl;
import net.tardis.mod.controls.DimensionControl;
import net.tardis.mod.controls.DoorControl;
import net.tardis.mod.controls.FacingControl;
import net.tardis.mod.controls.FastReturnControl;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.IncModControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.MonitorControl;
import net.tardis.mod.controls.RandomiserControl;
import net.tardis.mod.controls.RefuelerControl;
import net.tardis.mod.controls.SonicPortControl;
import net.tardis.mod.controls.StabilizerControl;
import net.tardis.mod.controls.TelepathicControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.controls.XControl;
import net.tardis.mod.controls.YControl;
import net.tardis.mod.controls.ZControl;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.tileentities.ConsoleTile;

public class ControlRegistry {

    public static final DeferredRegister<ControlEntry> CONTROLS = DeferredRegister.create(ControlEntry.class, Tardis.MODID);

    public static Supplier<IForgeRegistry<ControlEntry>> CONTROL_REGISTRY = CONTROLS.makeRegistry("control", () -> new RegistryBuilder<ControlEntry>().setMaxID(Integer.MAX_VALUE - 1));
    
    public static final RegistryObject<ControlEntry> THROTTLE = CONTROLS.register("throttle", () -> new ControlEntry(ThrottleControl::new));
    public static final RegistryObject<ControlEntry> HANDBRAKE = CONTROLS.register("handbrake", () -> new ControlEntry(HandbrakeControl::new));
    public static final RegistryObject<ControlEntry> RANDOM = CONTROLS.register("randomizer", () -> new ControlEntry(RandomiserControl::new));
    public static final RegistryObject<ControlEntry> DIMENSION = CONTROLS.register("dimension", () -> new ControlEntry(DimensionControl::new));
    public static final RegistryObject<ControlEntry> FACING = CONTROLS.register("facing", () -> ControlEntry.create(FacingControl::new));
    public static final RegistryObject<ControlEntry> X = CONTROLS.register("x", () -> ControlEntry.create(XControl::new));
    public static final RegistryObject<ControlEntry> Y = CONTROLS.register("y", () -> ControlEntry.create(YControl::new));
    public static final RegistryObject<ControlEntry> Z = CONTROLS.register("z", () -> ControlEntry.create(ZControl::new));
    public static final RegistryObject<ControlEntry> INC_MOD = CONTROLS.register("inc_mod", () -> ControlEntry.create(IncModControl::new));
    public static final RegistryObject<ControlEntry> LAND_TYPE = CONTROLS.register("land_type", () -> ControlEntry.create(LandingTypeControl::new));
    public static final RegistryObject<ControlEntry> REFUELER = CONTROLS.register("refueler", () -> ControlEntry.create(RefuelerControl::new));
    public static final RegistryObject<ControlEntry> FAST_RETURN = CONTROLS.register("fast_return", () -> ControlEntry.create(FastReturnControl::new));
    public static final RegistryObject<ControlEntry> TELEPATHIC = CONTROLS.register("telepathic", () -> ControlEntry.create(TelepathicControl::new));
    public static final RegistryObject<ControlEntry> STABILIZERS = CONTROLS.register("stabilizers", () -> ControlEntry.create(StabilizerControl::new));
    public static final RegistryObject<ControlEntry> SONIC_PORT = CONTROLS.register("sonic_port", () -> ControlEntry.create(SonicPortControl::new));
    public static final RegistryObject<ControlEntry> COMMUNICATOR = CONTROLS.register("communicator", () -> ControlEntry.create(CommunicatorControl::new));
    public static final RegistryObject<ControlEntry> DOOR = CONTROLS.register("door", () -> ControlEntry.create(DoorControl::new));
    public static final RegistryObject<ControlEntry> MONITOR = CONTROLS.register("monitor", () -> ControlEntry.create(MonitorControl::new));

    public static ControlEntry getControl(ResourceLocation name) {
        return CONTROL_REGISTRY.get().getValue(name);
    }
    
    /** An instance of a Control
     * <p> A Control is made up of a {@linkplain ControlEntity} and a logic handling class, that extends {@linkplain BaseControl} which uses a template defined by {@linkplain AbstractControl}
     * <br> The ControlEntity is the physical manifestation of the Control
     * <br> The logic handling of the control is all controlled by classes that extends {@linkplain AbstractControl}.
     * <p> To make a new Control, do the following:
     * <br> Make a {@linkplain DeferredRegister} which has your ModID and has ControlEntry as the class type
     * <br> Make a logic handling class that extends {@linkplain BaseControl}. Unless you need custom logic, BaseControl is enough.
     * <br>
    */
    public static class ControlEntry extends ForgeRegistryEntry<ControlEntry> {

        IControlFactory<AbstractControl> fact;

        public ControlEntry(IControlFactory<AbstractControl> fact) {
            this.fact = fact;
        }

        public static ControlEntry create(IControlFactory<AbstractControl> fact) {
            return new ControlEntry(fact);
        }

        public AbstractControl spawn(ConsoleTile console, ControlEntity entity) {
            AbstractControl control = fact.create(this, console, entity);
            return control;
        }

    }

    public interface IControlFactory<T extends AbstractControl> {

        T create(ControlEntry entry, ConsoleTile console, ControlEntity entity);
    }


}
