package net.tardis.mod.network.packets;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.network.packets.exterior.DoorData;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;
import net.tardis.mod.world.dimensions.TDimensions;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class SnapMessage {

	public SnapMessage() {}
	
	public static void encode(SnapMessage mes, PacketBuffer buf) {}
	
	public static SnapMessage decode(PacketBuffer buf) {
		return new SnapMessage();
	}
	
	public static void handle(SnapMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			ServerWorld world = context.get().getSender().getServerWorld();
			MinecraftServer server = context.get().getSender().getServer();
			if (WorldHelper.areDimensionTypesSame(world, TDimensions.DimensionTypes.TARDIS_TYPE)) {
				//If inside TARDIS
				TardisHelper.getConsoleInWorld(world).ifPresent(console -> {
					if(mes.canPlayerUseSnap(context.get().getSender(), console)){
						console.getDoor().ifPresent(door -> {
							boolean open = door.getOpenState() != EnumDoorState.CLOSED;
							door.setLocked(open);
							door.setOpenState(open ? EnumDoorState.CLOSED : EnumDoorState.BOTH);
							door.updateExteriorDoorData();
							playSnapSound(world, context.get().getSender().getPosition(), 0.5F, 1F);
						});
					}
				});
			}
			else { //If not in the Tardis dimension
				List<TileEntity> tiles = new ArrayList<>(world.loadedTileEntityList);
				//Cull tiles too far away
				tiles.removeIf(tile -> tile.getPos().distanceSq(context.get().getSender().getPosition()) > 20 * 20);


				for(TileEntity te : tiles){
					if(te instanceof ExteriorTile){
						ExteriorTile ext = (ExteriorTile)te;

						TardisHelper.getConsole(server, ext.getInteriorDimensionKey()).ifPresent(console -> {
							if(canPlayerUseSnap(context.get().getSender(), console)){

								boolean open = ext.getOpen() != EnumDoorState.CLOSED;
								ext.setLocked(open);
								ext.setDoorState(open ? EnumDoorState.CLOSED : EnumDoorState.BOTH);
								ext.copyDoorStateToInteriorDoor();
								ext.updateSpecific(DoorData.create(ext));
								playSnapSound(world, context.get().getSender().getPosition(), 0.5F, 1F);
							}
						});

					}
				}

			}
		});
		context.get().setPacketHandled(true);
	}

	public static boolean canPlayerUseSnap(PlayerEntity player, ConsoleTile tile){
		return tile.getEmotionHandler().getLoyalty(player.getUniqueID()) >= 100 && !tile.getInteriorManager().isInteriorStillRegenerating();
	}
	
	private static void playSnapSound(ServerWorld world, BlockPos pos, float volume, float pitch) {
		world.playSound(null, pos, TSounds.SNAP.get(), SoundCategory.BLOCKS, volume, pitch);
	}
}
