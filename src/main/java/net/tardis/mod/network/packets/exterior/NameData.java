package net.tardis.mod.network.packets.exterior;

import net.minecraft.network.PacketBuffer;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class NameData extends ExteriorData{

    public static final int LENGTH = 24;
    String name;

    public NameData(Type type){
        super(type);
    }

    public NameData(String name){
        this(Type.NAME);
        this.name = name;
    }

    @Override
    public void encode(PacketBuffer buf) {
        buf.writeString(this.name, LENGTH);
    }

    @Override
    public void decode(PacketBuffer buf) {
        this.name = buf.readString(LENGTH);
    }

    @Override
    public void apply(ExteriorTile tile) {
        tile.setCustomName(this.name);
    }
}
