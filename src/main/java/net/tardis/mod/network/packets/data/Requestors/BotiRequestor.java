package net.tardis.mod.network.packets.data.Requestors;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.tardis.mod.boti.IBotiEnabled;
import net.tardis.mod.boti.WorldShell;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.BOTIMessage;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class BotiRequestor extends Requestor<ExteriorTile>{

    public BotiRequestor() {
        super(ExteriorTile.class);
    }

    @Override
    public void act(ExteriorTile tile, ServerPlayerEntity sender) {
        WorldShell shell = tile.getBotiWorld();
        if(shell != null)
            Network.sendTo(new BOTIMessage(shell, tile.getPos()), sender);
    }
}
