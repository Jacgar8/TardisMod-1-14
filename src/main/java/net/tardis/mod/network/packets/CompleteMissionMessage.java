package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.experimental.advancement.TTriggers;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.missions.MiniMission;

public class CompleteMissionMessage {

	private BlockPos pos;
	
	public CompleteMissionMessage(BlockPos pos) {
		this.pos = pos;
	}
	
	public static void encode(CompleteMissionMessage mes, PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
	}
	
	public static CompleteMissionMessage decode(PacketBuffer buf) {
		return new CompleteMissionMessage(buf.readBlockPos());
	}
	
	public static void handle(CompleteMissionMessage mes, Supplier<NetworkEvent.Context> cont) {
		
		cont.get().enqueueWork(() -> {
			cont.get().getSender().getServerWorld().getCapability(Capabilities.MISSION).ifPresent(misCap -> {
				MiniMission mis = misCap.getMissionForPos(mes.pos);
				if(mis != null && !mis.getAwarded()) {
					TInventoryHelper.giveStackTo(cont.get().getSender(), mis.getReward());
					mis.setAwarded(true);
					if (mis.getCompletedAdvancementTrigger() != null && mis.getCompletedAdvancementTrigger() != TTriggers.DUMMY)
					    mis.getCompletedAdvancementTrigger().test(cont.get().getSender());
				}
			});
		});
		
		cont.get().setPacketHandled(true);
	}
}
