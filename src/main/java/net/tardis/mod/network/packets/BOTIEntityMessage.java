package net.tardis.mod.network.packets;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.boti.stores.EntityStorage;
import net.tardis.mod.client.ClientPacketHandler;

public class BOTIEntityMessage {

	public EntityStorage store;
	public BlockPos pos;
	public int type = 0;
	
	public BOTIEntityMessage(BlockPos pos, EntityStorage store) {
		this.pos = pos;
		this.type = 0;
		this.store = store;
	}
	
	public BOTIEntityMessage(EntityStorage store) {
		this.type = 1;
		this.store = store;
	}
	

	public static void encode(BOTIEntityMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.type);
		if(mes.type == 0) {
			buf.writeBlockPos(mes.pos);
		}
		buf.writeInt(mes.store.getStoreType().ordinal());
		mes.store.encode(buf);
		//Tardis.LOGGER.debug("Sent a BOTI Entity packet with a size of" + buf.readableBytes());
	}
	
	public static BOTIEntityMessage decode(PacketBuffer buf) {
		BlockPos pos = null;
		int type = buf.readInt();
        
		if(type == 0)
			pos = buf.readBlockPos();

		EntityStorage.EntityStorageTypes storeType = EntityStorage.EntityStorageTypes.values()[buf.readInt()];
		EntityStorage store = storeType.create(buf);
		
		if(type == 0)
			return new BOTIEntityMessage(pos, store);
		return new BOTIEntityMessage(store);
	}
	
	
	public static void handle(BOTIEntityMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			ClientPacketHandler.handleWorldShellEntity(mes.type, mes.store, mes.pos);
		});
		context.get().setPacketHandled(true);
	}
}
