package net.tardis.mod.vm;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.Util;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.sounds.TSounds;
/** Default implementation of AbstractVortexMFunction
 * <br> Extend this class for your custom functions
 * */
public class BaseVortexMFunction extends AbstractVortexMFunction{
	
	private String translationKey;

	@Override
	public void sendActionOnClient(World world, PlayerEntity player) {
		world.playSound(player, player.getPosition(), TSounds.VM_BUTTON.get(), SoundCategory.PLAYERS, 0.5F, 1F);
	}

	@Override
	public void sendActionOnServer(World world, ServerPlayerEntity player) {
		world.playSound(null, player.getPosition(), TSounds.VM_BUTTON.get(), SoundCategory.PLAYERS, 0.5F, 1F);
	}

	@Override
	public VortexMUseType getLogicalSide() {
		return VortexMUseType.BOTH;
	}

	@Override
	public String getTranslationKey() {
    	if (this.translationKey == null) {
    		this.translationKey = Util.makeTranslationKey("function.vm", this.getRegistryName());
    	}
    	return this.translationKey;
    }

	@Override
	public TranslationTextComponent getDisplayName() {
		return new TranslationTextComponent(this.getTranslationKey());
	}

}
