package net.tardis.mod.helper;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import com.mojang.authlib.GameProfile;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.util.text.event.ClickEvent;
import net.minecraft.util.text.event.HoverEvent;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.templates.FluidTank;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.ITardisWorldData;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.tileentities.ConsoleTile;

public class TextHelper {

    public static TextComponent createTextComponentWithTip(String text, String tooltipText) {
        //Always surround tool tip items with brackets
        TextComponent textComponent = new StringTextComponent("[" + text + "]");
        textComponent.modifyStyle(style -> {
            return style.setFormatting(TextFormatting.GREEN)//color tool tip items green
                    .setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new StringTextComponent(tooltipText)))
                    .setClickEvent(new ClickEvent(ClickEvent.Action.COPY_TO_CLIPBOARD, tooltipText));
        });
        return textComponent;
    }
    
    public static TextComponent createTextComponentWithTipSuggest(String text, String tooltipText) {
        //Always surround tool tip items with brackets
        TextComponent textComponent = new StringTextComponent("[" + text + "]");
        textComponent.modifyStyle(style -> {
            return style.setFormatting(TextFormatting.GREEN)//color tool tip items green
                    .setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new StringTextComponent(tooltipText)))
                    .setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, tooltipText));
        });
        return textComponent;
    }
    
    /** Creates a Text Component but doesn't allow copying of the tooltip text. Instead, will copy the text in the text component*/
    public static TextComponent createTextWithoutTooltipCopying(String text, String tooltipText) {
        //Always surround tool tip items with brackets
        TextComponent textComponent = new StringTextComponent("[" + text + "]");
        textComponent.modifyStyle(style -> {
            return style.setFormatting(TextFormatting.GREEN)//color tool tip items green
                    .setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new StringTextComponent(tooltipText)))
                    .setClickEvent(new ClickEvent(ClickEvent.Action.COPY_TO_CLIPBOARD, text));
        });
        return textComponent;
    }
    
    public static TextComponent createTextWithoutTooltip(String text) {
        //Always surround tool tip items with brackets
        TextComponent textComponent = new StringTextComponent("[" + text + "]");
        textComponent.modifyStyle(style -> {
            return style.setFormatting(TextFormatting.GREEN)//color tool tip items green
                    .setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, text));
        });
        return textComponent;
    }

    /**
     * For getting tardis tool tip when you already have the data there
     */
    public static TextComponent getTardisTextObject(ServerWorld world, ITardisWorldData data) {
        return createTextComponentWithTip(data.getTARDISName(), world.getDimensionKey().getLocation().toString());
    }

    /**
     * Gets a Tardis UUID based dimension id and also shows tooltip which displays the Tardis display name
     * <br> If clicked, allows user to copy the dimension id
     * @param world
     * @param data
     * @return
     */
    public static TextComponent getTardisDimObject(ServerWorld world, ITardisWorldData data) {
        return createTextWithoutTooltipCopying(world.getDimensionKey().getLocation().toString(), data.getTARDISName());
    }

    /**
     * For getting tardis tool tip when you only have the world related to it.
     */
    public static TextComponent getTardisTextObject(ServerWorld world) {
        Optional<ConsoleTile> console = TardisHelper.getConsoleInWorld(world);

        final TextComponent[] tardisTextObject = new TextComponent[1];

        console.ifPresent(tile -> tile.getWorld().getCapability(Capabilities.TARDIS_DATA)
                .ifPresent(data -> tardisTextObject[0] = getTardisTextObject(world, data)));

        return tardisTextObject != null // if we were able to retrieve it
                ? tardisTextObject[0] // then use it
                : new StringTextComponent(world.getDimensionKey().getLocation().toString());
    }
    
    /** Gets the Tardis UUID based dimension id and its corresponding name in a tooltip if we only have a world context*/
    public static TextComponent getTardisDimObject(ServerWorld world) {
        Optional<ConsoleTile> console = TardisHelper.getConsoleInWorld(world);

        final TextComponent[] tardisTextObject = new TextComponent[1];

        console.ifPresent(tile -> tile.getWorld().getCapability(Capabilities.TARDIS_DATA)
                .ifPresent(data -> tardisTextObject[0] = getTardisDimObject(world, data)));

        return tardisTextObject[0] != null // if we were able to retrieve it
                ? tardisTextObject[0] // then use it
                : new StringTextComponent(world.getDimensionKey().getLocation().toString());
    }


    /**
     * For getting player tool tip. If player has been online since server start, then will show their display name.
     */
    public static TextComponent getPlayerTextObject(ServerWorld world, UUID id) {
        GameProfile profileByUUID = world.getServer().getPlayerProfileCache().getProfileByUUID(id);
        String playerName = profileByUUID != null ? profileByUUID.getName() : "OFFLINE Player";
        return createTextComponentWithTip(playerName, id.toString());
    }
    
    public static TextComponent getBlockPosObject(BlockPos pos) {
    	String formattedPos = WorldHelper.formatBlockPosDebug(pos);
    	return createTextWithoutTooltip(formattedPos);
    }
    
    public static List<ITextComponent> combineMultipleComponents(ITextComponent... components) {
    	List<ITextComponent> allComponents = new ArrayList<>();
    	for (ITextComponent component : components) {
    	        allComponents.add(component);
    	}
    	return allComponents;
    }

    /** Creates a tooltip for getting fluid in a fluid tank
     * <br> TODO: Make a generic Fluid Tank wrapper and Fluid Tank widget that handles it for us instead of requiring this helper*/
    public static List<ITextComponent> createFluidTankToolTip(FluidTank tank) {
	    if (tank.getFluid().isEmpty()) {
	        return TextHelper.combineMultipleComponents(TardisConstants.ContainerComponentTranslations.EMPTY_FLUID);
	    } else {
	        DecimalFormat decimalFormat = new DecimalFormat();
	        FluidStack fluid = tank.getFluid();
	        IFormattableTextComponent fluidComponent = new TranslationTextComponent(TardisConstants.Strings.FLUID).modifyStyle(style -> style.setFormatting(TextFormatting.GOLD)).appendString(getFluidName(fluid));
	        IFormattableTextComponent component = new StringTextComponent(decimalFormat.format(tank.getFluidAmount())).mergeStyle(TextFormatting.GOLD)
	                .appendSibling(TardisConstants.ContainerComponentTranslations.FORWARD_SLASH)
	                .appendSibling(new StringTextComponent(decimalFormat.format(tank.getCapacity())).modifyStyle(style -> style.setFormatting(TextFormatting.GOLD))
	                .appendSibling(TardisConstants.Suffix.MILLIBUCKETS));
	        return TextHelper.combineMultipleComponents(fluidComponent, component);
	    }
	}
    /** Special case fluid tank tooltip to account for a hardcoded case like the alembic*/
    public static List<ITextComponent> createSpecialCaseFluidTankToolTip(int currentFluid, int maxFluid, IFormattableTextComponent fluidName) {
	    if (currentFluid == 0) {
	        return TextHelper.combineMultipleComponents(TardisConstants.ContainerComponentTranslations.EMPTY_FLUID);
	    } else {
	        DecimalFormat decimalFormat = new DecimalFormat();
	        IFormattableTextComponent fluidComponent = new TranslationTextComponent(TardisConstants.Strings.FLUID).modifyStyle(style -> style.setFormatting(TextFormatting.GOLD)).appendSibling(fluidName);
	        IFormattableTextComponent component = new StringTextComponent(decimalFormat.format(currentFluid)).mergeStyle(TextFormatting.GOLD)
	                .appendSibling(TardisConstants.ContainerComponentTranslations.FORWARD_SLASH)
	                .appendSibling(new StringTextComponent(decimalFormat.format(maxFluid)).modifyStyle(style -> style.setFormatting(TextFormatting.GOLD))
	                .appendSibling(TardisConstants.Suffix.MILLIBUCKETS));
	        return TextHelper.combineMultipleComponents(fluidComponent, component);
	    }
	}

	public static String getFluidName(FluidStack stack) {
	    return new TranslationTextComponent(stack.getFluid().getAttributes().getTranslationKey(stack)).getString();
	}
	
	public static TranslationTextComponent getFluidNameTranslation(FluidStack stack) {
	    return new TranslationTextComponent(stack.getFluid().getAttributes().getTranslationKey(stack));
	}

	/**
	 * Helper to create text list for making a tooltip for progress bars
	 * <br> Shows progress in ticks  and ETA time
	 * @param currentProgress
	 * @param maxProgressTicks
	 * @param staticDisplay - If we are displaying it at a particular point, such as in JEI
	 * @return
	 */
	public static List<ITextComponent> createProgressBarTooltip(int currentProgress, int maxProgressTicks, boolean staticDisplay){
	    DecimalFormat decimalFormat = new DecimalFormat();
	    TextComponent currentProgressText = new StringTextComponent(decimalFormat.format(currentProgress));
	    currentProgressText.modifyStyle(style -> style.setFormatting(TextFormatting.WHITE));
	    TextComponent maxProgressText = new StringTextComponent(decimalFormat.format(maxProgressTicks));
	    maxProgressText.modifyStyle(style -> style.setFormatting(TextFormatting.WHITE));
	    
	    IFormattableTextComponent progress = new TranslationTextComponent(TardisConstants.Strings.PROGRESS).modifyStyle(style -> style.setFormatting(TextFormatting.GOLD));
	    progress.appendSibling(currentProgressText)
	    		.appendSibling(TardisConstants.ContainerComponentTranslations.FORWARD_SLASH)
	    		.appendSibling(maxProgressText);
	    
	    IFormattableTextComponent eta = new TranslationTextComponent(TardisConstants.Strings.ETA_TIME).modifyStyle(style -> style.setFormatting(TextFormatting.GOLD));
	    double time = (maxProgressTicks - currentProgress)/ 20.0D;
	    double etaTime = currentProgress <= maxProgressTicks && currentProgress != 0 ? time : (staticDisplay ? time : 0);
	    TextComponent maxEtaText = new StringTextComponent(decimalFormat.format(Math.ceil(etaTime)));
	    maxEtaText.modifyStyle(style -> style.setFormatting(TextFormatting.WHITE));
	    IFormattableTextComponent seconds = new TranslationTextComponent(TardisConstants.Strings.SECONDS);
	    
	    eta.appendSibling(maxEtaText).appendSibling(seconds);
	    return TConfig.CLIENT.showMoreMachineTooltips.get() ? combineMultipleComponents(progress, eta) : combineMultipleComponents(eta);
	}
	
	public static List<ITextComponent> createProgressBarTooltip(int currentProgress, int maxProgressTicks){
		return createProgressBarTooltip(currentProgress, maxProgressTicks, false);
	}
	
	/** Version of createProgressBarTooltip for machines which only expose a percentage
	 * <br> Only shows progress as a percentage, no ETA time*/
	public static List<ITextComponent> createProgressBarTooltipPercentage(float progressPercent){
	    DecimalFormat decimalFormat = new DecimalFormat();
	    double currentProgress = (double)progressPercent * 100;
	    int maxProgress = 100;
	    TextComponent currentProgressText = new StringTextComponent(decimalFormat.format(currentProgress));
	    currentProgressText.modifyStyle(style -> style.setFormatting(TextFormatting.WHITE));
	    TextComponent maxProgressText = new StringTextComponent(decimalFormat.format(maxProgress));
	    maxProgressText.modifyStyle(style -> style.setFormatting(TextFormatting.WHITE));
	    
	    IFormattableTextComponent progress = new TranslationTextComponent(TardisConstants.Strings.PROGRESS).modifyStyle(style -> style.setFormatting(TextFormatting.GOLD));
	    progress.appendSibling(currentProgressText)
	    		.appendSibling(TardisConstants.ContainerComponentTranslations.FORWARD_SLASH)
	    		.appendSibling(maxProgressText);
	    return combineMultipleComponents(progress);
	}
	
	/** Prepends the word "description" to the input text and colours the input text gray
	 * <br> For use in tooltips, create a final field that stores this instance to prevent new instances being created every render tick*/
	public static IFormattableTextComponent createDescriptionItemTooltip(IFormattableTextComponent descriptionTooltip) {
		return TardisConstants.Prefix.TOOLTIP_ITEM_DESCRIPTION.deepCopy().appendSibling(descriptionTooltip.mergeStyle(TextFormatting.GRAY));
	}
	
	/** Colours the input text gray. Useful for the extra tooltip lines on an item tooltip*/
	public static IFormattableTextComponent createExtraLineItemTooltip(IFormattableTextComponent descriptionTooltip) {
		return descriptionTooltip.mergeStyle(TextFormatting.GRAY).deepCopy();
	}

	/** Creates a text component with the Vortex Manipulator header text*/
	public static IFormattableTextComponent createVortexManipMessage(IFormattableTextComponent descriptionTooltip) {
		return TardisConstants.Prefix.VORTEX_M_HEADER.deepCopy().appendString("\n").appendSibling(descriptionTooltip.deepCopy());
	}
	
	public static List<ITextComponent> getSchematicsInList(List<Schematic> schematics){
	    Set<Schematic> distinctSchematics = new HashSet<>();
	    distinctSchematics.addAll(schematics);

	    List<ITextComponent> schematicTexts = new ArrayList<>();
	    distinctSchematics.forEach(entry -> {
	        int count = Collections.frequency(schematics, entry);
	        if (entry != null)
	            schematicTexts.add(new StringTextComponent("- " + entry.getDisplayName()).appendSibling(new TranslationTextComponent(TardisConstants.Translations.COUNT_TRANSLATION_KEY, count)));
	    });
	    
	    return schematicTexts;
	}
}
