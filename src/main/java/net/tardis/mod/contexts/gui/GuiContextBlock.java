package net.tardis.mod.contexts.gui;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.tardis.mod.misc.GuiContext;

public class GuiContextBlock extends GuiContext {

	public World world;
	public BlockPos pos;
	
	public GuiContextBlock(World world, BlockPos pos) {
		this.world = world;
		this.pos = pos;
	}
}
