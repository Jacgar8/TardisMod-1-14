package net.tardis.mod.traits;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.item.ArmorStandEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;

public class PeaceTrait extends AbstractEntityDeathTrait{

	public PeaceTrait(TardisTraitType type) {
		super(type);
	}

	@Override
	public void tick(ConsoleTile tile) {}

	@Override
	public void onPlayerKilledEntity(PlayerEntity owner, ConsoleTile tardis, LivingEntity victim) {
		double mod = 1.0 - this.getWeight();
		if(tardis.getEmotionHandler().getMood() > EnumHappyState.SAD.getTreshold()) {
			if (!(victim instanceof ArmorStandEntity)) {//Exclude Armor Stands because losing mood from "killing" them does not make sense
				if (victim instanceof MobEntity) {
					MobEntity mobVictim = (MobEntity)victim;
					//Ignore mob entities that were aggresive or had an attack target.
					if (mobVictim.isAggressive() || mobVictim.getAttackTarget() != null) {
						return;
					}
				}
				else {
					//If the victim had a revenge target, then it is probably wanting revenge or is fleeing from the attack, lose mood. 
					//We need to do a null check even though this method is nullable because Mojang doesn't actually do a null check themselves when using it elsewhere, which can cause a crash.
					if (victim.getRevengeTarget() != null){
						tardis.getEmotionHandler().addLoyalty(owner, -(3 + (int)Math.ceil(3 * mod)));
						tardis.getEmotionHandler().addMood(-(7 + (int)Math.ceil(7 * mod)));
						this.warnPlayer(tardis, TardisTrait.GENERIC_DISLIKE);
					}
				}
			}
		}
	}

}
